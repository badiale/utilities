package br.com.dragonrise.filesizeanalyzer;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

public class FileTreeCreator {
	private static Logger logger = LogManager
			.getLogger ( FileTreeCreator.class );

	public static class TreeResult {
		final private FileNode root;
		final private List<File> errors;

		private TreeResult ( final FileNode root, final List<File> errors ) {
			this.root = checkNotNull ( root, "root is null" );
			this.errors = Lists.newArrayList ( errors );
		}

		public FileNode getRootNode ( ) {
			return root;
		}

		public List<File> getFilesWithError ( ) {
			return Collections.unmodifiableList ( errors );
		}
	}

	private FileTreeCreator ( ) {
	}

	public static TreeResult createTree ( final File file ) {
		TreeResult ret = null;
		if ( file.isDirectory ( ) ) {
			ret = createDirectoryNode ( file );
		} else {
			ret = new TreeResult ( new FileLeaf ( file ),
					Collections.<File> emptyList ( ) );
		}
		return ret;
	}

	private static File[] emptyFileArray = new File[] {};

	private static TreeResult createDirectoryNode ( final File directory ) {
		if ( !directory.isDirectory ( ) ) {
			throw new IllegalStateException ( "file is not an directory: "
					+ directory );
		}

		File[] listFiles = directory.listFiles ( );
		final List<File> filesWithError = Lists.newLinkedList ( );
		final List<FileNode> childs = Lists.newLinkedList ( );

		if ( listFiles == null ) {
			listFiles = emptyFileArray;
			logger.warn ( "Error reading files from " + directory );
			filesWithError.add ( directory );
		}

		for ( final File f : listFiles ) {
			if ( !f.exists ( ) ) {
				logger.warn ( "file " + f + " doesn't exists" );
			}
			final TreeResult child = createTree ( f );
			filesWithError.addAll ( child.getFilesWithError ( ) );
			childs.add ( child.getRootNode ( ) );
		}

		return new TreeResult ( new DirectoryNode ( directory, childs ),
				filesWithError );
	}
}
