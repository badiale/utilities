package br.com.dragonrise.filesizeanalyzer.gui;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.dragonrise.filesizeanalyzer.FileNode;
import br.com.dragonrise.filesizeanalyzer.FileTreeCreator.TreeResult;

public class FileResultTreeModel
		implements
			TreeModel {

	private static final Logger logger = LogManager
			.getLogger ( FileResultTreeModel.class );

	private final TreeResult result;

	public FileResultTreeModel ( final TreeResult result ) {
		this.result = checkNotNull ( result, "result is null" );
	}

	public FileNode getRoot ( ) {
		logger.trace ( "getRoot" );
		return result.getRootNode ( );
	}

	public FileNode getChild ( final Object parent, final int index ) {
		logger.trace ( "getChild" );
		final List<FileNode> childs = ( (FileNode) parent ).getChilds ( );
		if ( ( childs.size ( ) <= index ) || ( index < 0 ) ) {
			return null;
		}

		return childs.get ( index );
	}

	public int getChildCount ( final Object parent ) {
		logger.trace ( "getChildCount" );
		final FileNode root = (FileNode) parent;
		if ( isLeaf ( root ) ) {
			return 0;
		}
		final int size = root.getChilds ( ).size ( );
		return size;
	}

	public boolean isLeaf ( final Object node ) {
		logger.trace ( "isLeaf" );
		final boolean leaf = ( (FileNode) node ).isLeaf ( );
		return leaf;
	}

	public void
			valueForPathChanged ( final TreePath path, final Object newValue ) {
		logger.trace ( "valueForPathChanged" );
		throw new UnsupportedOperationException (
				"method valueForPathChanged not yet implemented" );
	}

	public int getIndexOfChild ( final Object parent, final Object child ) {
		logger.trace ( "getIndexOfChild" );
		final int index = ( (FileNode) parent ).getChilds ( ).indexOf ( child );
		return index;
	}

	public void addTreeModelListener ( final TreeModelListener l ) {
		logger.trace ( "addTreeModelListener" );
	}

	public void removeTreeModelListener ( final TreeModelListener l ) {
		logger.trace ( "removeTreeModelListener" );
	}
}
