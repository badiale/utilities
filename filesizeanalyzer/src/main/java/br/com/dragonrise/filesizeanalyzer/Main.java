package br.com.dragonrise.filesizeanalyzer;

import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import br.com.dragonrise.filesizeanalyzer.gui.MainFrame;

public class Main {

	/**
	 * @param args
	 */
	public static void main ( final String[] args ) {
		SwingUtilities.invokeLater ( new Runnable ( ) {
			public void run ( ) {
				Main.showFrame ( );
			}
		} );
	}

	private static void showFrame ( ) {
		final MainFrame mainFrame = new MainFrame ( "File Size Analyzer" );
		mainFrame.setVisible ( true );
		mainFrame.setDefaultCloseOperation ( WindowConstants.DISPOSE_ON_CLOSE );
	}

}
