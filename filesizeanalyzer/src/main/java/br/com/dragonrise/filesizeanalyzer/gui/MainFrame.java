package br.com.dragonrise.filesizeanalyzer.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class MainFrame
		extends
			JFrame {

	private static final long serialVersionUID = 3031443750080992192L;

	final private ResultsPanel resultsPanel;

	public MainFrame ( final String title ) {
		super ( title );

		final JMenuBar menuBar = new JMenuBar ( );
		add ( menuBar, BorderLayout.NORTH );

		final JMenu fileMenu = new JMenu ( "File" );
		menuBar.add ( fileMenu );

		final JMenuItem menuItemOpen = new JMenuItem ( new AbstractAction ( ) {
			private static final long serialVersionUID = 1928244944319539415L;

			public void actionPerformed ( final ActionEvent e ) {
				analyzeLocation ( );
			}
		} );
		menuItemOpen.setText ( "Analyze location..." );
		fileMenu.add ( menuItemOpen );

		final StatusBar progress = new StatusBar ( );
		add ( progress, BorderLayout.SOUTH );

		resultsPanel = new ResultsPanel ( progress );
		add ( resultsPanel, BorderLayout.CENTER );

		setSize ( 800, 600 );
		setLocationRelativeTo ( null );
	}

	private void analyzeLocation ( ) {
		final JFileChooser fileChooser = new JFileChooser ( );
		fileChooser.setFileSelectionMode ( JFileChooser.DIRECTORIES_ONLY );
		final int returnVal = fileChooser.showOpenDialog ( this );

		if ( returnVal == JFileChooser.APPROVE_OPTION ) {
			final File file = fileChooser.getSelectedFile ( );
			if ( file.exists ( ) && file.isDirectory ( ) ) {
				resultsPanel.analyzeFile ( file );
				revalidate ( );
				repaint ( );
			} else {
				JOptionPane
						.showMessageDialog (
								this,
								"The selected file doesn't exists, or is'nt a directory.",
								"Invalid selection", JOptionPane.ERROR_MESSAGE );
			}
		}
	}
}
