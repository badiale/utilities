package br.com.dragonrise.filesizeanalyzer.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class StatusBar
		extends
			JPanel {
	private static final long serialVersionUID = -860881520407252792L;

	private final JLabel statusText;
	private final JLabel secondaryText;
	private final JProgressBar progressBar;

	public StatusBar ( ) {
		statusText = createStatusText ( );
		secondaryText = createSecondaryText ( );
		progressBar = createProgressBar ( );

		setLayout ( new BorderLayout ( ) );

		final JPanel texts = new JPanel ( new BorderLayout ( ) );
		texts.add ( statusText );
		texts.add ( secondaryText, BorderLayout.LINE_END );
		add ( texts );

		add ( progressBar, BorderLayout.LINE_END );
	}

	private JLabel createStatusText ( ) {
		return new JLabel ( );
	}

	private JLabel createSecondaryText ( ) {
		final JLabel ret = new JLabel ( );
		ret.setPreferredSize ( new Dimension ( 150, 20 ) );
		ret.setHorizontalAlignment ( SwingConstants.RIGHT );
		return ret;
	}

	private JProgressBar createProgressBar ( ) {
		return new JProgressBar ( );
	}

	public void setStatusText ( final String text ) {
		statusText.setText ( text );
	}

	public void setSecondaryText ( final String text ) {
		secondaryText.setText ( text );
	}

	public String getStatusText ( ) {
		return statusText.getText ( );
	}

	public String getSecondaryText ( ) {
		return secondaryText.getText ( );
	}

	public void startProgressBar ( ) {
		progressBar.setIndeterminate ( true );
	}

	public void startProgressBar ( final int maxValue ) {
		progressBar.setMaximum ( maxValue );
	}

	public void stopProgressBar ( ) {
		setProgress ( 0 );
		progressBar.setIndeterminate ( false );
	}

	public void setProgress ( final int value ) {
		progressBar.setValue ( value );
	}

	public void cleanStatus ( ) {
		setStatusText ( null );
		setSecondaryText ( null );
		stopProgressBar ( );
	}
}
