package br.com.dragonrise.filesizeanalyzer;

import java.io.File;
import java.util.List;

public interface FileNode
		extends
			Comparable<FileNode> {

	public File getFile ( );

	public FileSize getNodeSize ( );

	public List<FileNode> getChilds ( );

	public boolean isLeaf ( );

	public int getTotalChildCount ( );
}
