package br.com.dragonrise.filesizeanalyzer;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class FileSize
		extends
			Number
		implements
			Comparable<FileSize> {

	private static final long serialVersionUID = 7185865463643725442L;

	private final long bytes;
	private final static FileSize ZERO = new FileSize ( 0 );

	private FileSize ( final long bytes ) {
		this.bytes = bytes;
	}

	public static FileSize valueOf ( final long bytes ) {
		if ( bytes == 0 ) {
			return ZERO;
		}
		return new FileSize ( bytes );
	}

	public long asBytes ( ) {
		return bytes;
	}

	public double asKiloBytes ( ) {
		return divideBytes ( 1 );
	}

	public double asMegaBytes ( ) {
		return divideBytes ( 2 );
	}

	public double asGigaBytes ( ) {
		return divideBytes ( 3 );
	}

	public double asTeraBytes ( ) {
		return divideBytes ( 4 );
	}

	public int getBytes ( ) {
		return calculateBytes ( asBytes ( ) );
	}

	public int getKiloBytes ( ) {
		return calculateBytes ( asKiloBytes ( ) );
	}

	public int getMegaBytes ( ) {
		return calculateBytes ( asMegaBytes ( ) );
	}

	public int getGigaBytes ( ) {
		return calculateBytes ( asGigaBytes ( ) );
	}

	public int getTeraBytes ( ) {
		return calculateBytes ( asTeraBytes ( ) );
	}

	private double divideBytes ( final int unit ) {
		return bytes / Math.pow ( 1024, unit );
	}

	private int calculateBytes ( final double totalSize ) {
		final double initial = totalSize / 1024;
		return (int) ( ( initial - ( (long) initial ) ) * 1024 );
	}

	public int compareTo ( final FileSize other ) {
		return Long.compare ( bytes, other.bytes );
	}

	public FileSize add ( final FileSize other ) {
		return new FileSize ( bytes + other.bytes );
	}

	public FileSize sub ( final FileSize other ) {
		return new FileSize ( bytes - other.bytes );
	}

	@Override
	public int intValue ( ) {
		return (int) longValue ( );
	}

	@Override
	public long longValue ( ) {
		return bytes;
	}

	@Override
	public float floatValue ( ) {
		return longValue ( );
	}

	@Override
	public double doubleValue ( ) {
		return longValue ( );
	}

	@Override
	public boolean equals ( final Object obj ) {
		if ( obj == this ) {
			return true;
		}

		if ( ! ( obj instanceof FileSize ) ) {
			return false;
		}

		final FileSize other = (FileSize) obj;
		return new EqualsBuilder ( ).append ( bytes, other.bytes ).isEquals ( );
	}

	@Override
	public int hashCode ( ) {
		return new HashCodeBuilder ( ).append ( bytes ).toHashCode ( );
	}

	@Override
	public String toString ( ) {
		final long bytes = getBytes ( );
		final long kilo = getKiloBytes ( );
		final long mega = getMegaBytes ( );
		final long giga = getGigaBytes ( );
		final long tera = getTeraBytes ( );

		final StringBuilder ret = new StringBuilder ( );

		if ( tera > 0 ) {
			ret.append ( tera ).append ( "T " );
		}

		if ( giga > 0 ) {
			ret.append ( giga ).append ( "G " );
		}

		if ( mega > 0 ) {
			ret.append ( mega ).append ( "M " );
		}

		if ( kilo > 0 ) {
			ret.append ( kilo ).append ( "K " );
		}

		if ( bytes > 0 ) {
			ret.append ( bytes ).append ( "B " );
		}

		return ret.length ( ) == 0 ? "0B " : ret.toString ( );
	}
}
