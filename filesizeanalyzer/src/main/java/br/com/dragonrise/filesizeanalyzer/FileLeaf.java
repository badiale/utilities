package br.com.dragonrise.filesizeanalyzer;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileLeaf
		implements
			FileNode {

	private static final Logger logger = LogManager.getLogger ( FileLeaf.class );

	private final File file;

	private transient FileSize fileSize = null;
	private transient int hashCode = -1;
	private transient String toString = null;

	public FileLeaf ( final File file ) {
		this.file = checkNotNull ( file, "file is null" );
	}

	public File getFile ( ) {
		return file;
	}

	public FileSize getNodeSize ( ) {
		logger.trace ( "getNodeSize" );
		FileSize ret = fileSize;
		if ( ret == null ) {
			ret = FileSize.valueOf ( file.length ( ) );
		}
		return ret;
	}

	public List<FileNode> getChilds ( ) {
		logger.trace ( "getChilds" );
		return Collections.emptyList ( );
	}

	public boolean isLeaf ( ) {
		logger.trace ( "isLeaf" );
		return true;
	}

	public int getTotalChildCount ( ) {
		logger.trace ( "getTotalChildCount" );
		return 0;
	}

	public int compareTo ( final FileNode other ) {
		return file.compareTo ( other.getFile ( ) );
	}

	@Override
	public boolean equals ( final Object obj ) {
		logger.trace ( "equals" );
		if ( obj == this ) {
			return true;
		}

		if ( ! ( obj instanceof FileLeaf ) ) {
			return false;
		}

		final FileLeaf other = (FileLeaf) obj;
		return new EqualsBuilder ( ).append ( file, other.file ).isEquals ( );
	}

	@Override
	public int hashCode ( ) {
		logger.trace ( "hashCode" );
		int ret = hashCode;
		if ( ret < 0 ) {
			ret = new HashCodeBuilder ( ).append ( file ).toHashCode ( );
		}
		return ret;
	}

	@Override
	public String toString ( ) {
		logger.trace ( "toString" );
		String ret = toString;
		if ( ret == null ) {
			ret = file.getName ( );
		}
		return ret;
	}
}
